# Superlive

![coverage][coverage_badge]
[![style: very good analysis][very_good_analysis_badge]][very_good_analysis_link]
[![License: MIT][license_badge]][license_link]

Superlive the one you need.

---

## Getting Started 🚀

This project contains 3 flavors:

- development
- staging
- production

To run the desired flavor either use the launch configuration in VSCode/Android Studio or use the
following commands:

```sh
# Development
$ flutter run --flavor development --target lib/main_development.dart

# Staging
$ flutter run --flavor staging --target lib/main_staging.dart

# Production
$ flutter run --flavor production --target lib/main_production.dart
```

_\*Tech works on iOS, Android._

---

## TextTheme TextStyle

```dart

TextTheme(
  displayLarge: TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.bold,
  ),
  displayMedium: TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w600,
  ),
  displaySmall: TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w600,
  ),
  headlineLarge: TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w600,
  ),
  headlineMedium: TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w600,
  ),
  headlineSmall: TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w600,
  ),
  titleLarge: TextStyle(
    fontSize: 17,
    fontWeight: FontWeight.w400,
  ),
  titleMedium: TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w400,
  ),
  titleSmall: TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w400,
  ),
  bodyLarge: TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
  ),
  bodyMedium: TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w400,
  ),
  bodySmall: TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w400,
  ),
  labelLarge: TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w400,
  ),
  labelMedium: TextStyle(
    fontSize: 11,
    fontWeight: FontWeight.w400,
  ),
  labelSmall: TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w400,
  ),
);
```

### Project Structure

```
├── lib
│   ├── l10n
│   │   ├── arb
│   │   │   ├── app_en.arb
│   │   │   └── app_es.arb
│   │   └── l10n.dart
│   ├── src
│   │   ├── app
│   │   │   ├── firebase_options
│   │   │   │   ├── firebase_options.dev.dart
│   │   │   │   ├── firebase_options.prod.dart
│   │   │   │   └── firebase_options.stg.dart
│   │   │   ├── app.dart
│   │   │   └── route_active_mixin.dart
│   │   ├── common
│   │   │   ├── ....
│   │   │   └──  index.dart
│   │   ├── data
│   │   │   ├── model
│   │   │   │   ├── ....
│   │   │   │   └── index.dart
│   │   │   ├── service
│   │   │   │   ├── ....
│   │   │   │   └── index.dart
│   │   │   └── index.dart
│   │   ├── dependency_injections
│   │   │   └── application_service.dart
│   │   ├── logic
│   │   │   ├── bloc
│   │   │   │   ├── ....
│   │   │   │   └── index.dart
│   │   │   ├── cubit 
│   │   │   │   ├── ....
│   │   │   │   └── index.dart
│   │   │   └── index.dart
│   │   ├── view
│   │   │   ├── paage
│   │   │   │   ├── ....
│   │   │   │   └── index.dart
│   │   │   ├── route 
│   │   │   │   └── page_routes.dart
│   │   │   ├── widget
│   │   │   │   ├── ....
│   │   │   │   └── index.dart
│   │   │   └── index.dart
│   │   └── bloc_providers.dart
│   ├── bootstrap.dart
│   ├── index.dart
│   ├── main_development.dart
│   ├── main_production.dart
│   └── main_staging.dart

```

## Page

```dart
import 'package:tech/index.dart';

class NamePage extends StatefulWidget {
  static const String routeName = '/name_page';

  const NamePage({Key? key}) : super(key: key);

  @override
  State<NamePage> createState() => _NamePageState();
}

class _NamePageState extends State<NamePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: OneLeadingAppbar(),
        title: Text(context.l10n.pageName),
      ),
      body: Container(),
    );
  }
}
```

---

## Service

```dart
import 'package:dartz/dartz.dart';
import 'package:tech/index.dart';

class NameService {
  OneResponseEither<NameResModel> action(NameReqModel req) async {
    try {
      final value = await OneHttp(
        url: OnePath.urlName,
        fields: req.toJson(),
      ).post();
      var response = OneResponse<NameResModel>.fromJson(
          value, (data) => NameResModel.fromJson(data));
      return right(response.data!);
    } catch (e) {
      print(e);
      return left(e.toString());
    }
  }

  OneResponseEither<OneResponsePaginateData<NameResModel>> getList(int page) async {
    try {
      final value = await OneHttp(
        url: OnePath.urlName,
        fields: {
          'page': page,
        },
      ).get();
      var response = OneResponsePaginate<NameResModel>.fromJson(
          value, (data) => NameResModel.fromJson(data));
      return right(response.data);
    } catch (e) {
      print(e);
      return left(e.toString());
    }
  }
}

```

---

## Cubit

### 1 State

```dart
part of 'name_cubit.dart';

class NameState {
  NameState({
    this.stateStatus: OneStateStatus.none,
    this.message,
    this.data,
  });

  final OneStateStatus stateStatus;
  final String? message;
  final NameResModel? data;

  NameState copyWith({
    OneStateStatus? stateStatus,
    String? message,
    NameResModel? data,
  }) {
    return NameState(
      stateStatus: stateStatus ?? this.stateStatus,
      message: message ?? this.message,
      data: data ?? this.data,
    );
  }
}
```

### 2 Cubit

```dart
import 'package:tech/index.dart';

part 'name_state.dart';

class NameCubit extends Cubit<NameState> {
  NameCubit() : super(NameState());
  final NameService _service = AppService.I.get<NameService>();

  void action(NameReqModel req) async {
    if (state.stateStatus == OneStateStatus.loading) return;
    emit(state.copyWith(
      stateStatus: OneStateStatus.loading,
    ));
    final response = await _service.action(req);
    emit(response.fold(
          (l) =>
          state.copyWith(
            stateStatus: OneStateStatus.failure,
            message: l,
          ),
          (r) {
        return state.copyWith(
          stateStatus: OneStateStatus.success,
          data: r,
        );
      },
    ));
  }

  void reset() {
    emit(NameState());
  }
}
```

### 4 Page

```dart
import 'package:tech/index.dart';

class NamePage extends StatefulWidget {
  static const String routeName = '/name_page';

  const NamePage({Key? key}) : super(key: key);

  @override
  State<NamePage> createState() => _NamePageState();
}

class _NamePageState extends State<NamePage> with RouteActiveMixin<NamePage> {
  late final NameCubit _cubit;

  @override
  void initState() {
    _cubit = context.read<NameCubit>();

    /// CALL ACTION ::::::::::::::::::::::::::::::::::
    _cubit.action(new NameReqModel());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _cubit.reset();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: OneLeadingAppbar(),
        title: Text(context.l10n.pageName),
      ),
      body: BlocConsumer<NameCubit, NameState>(
        listener: (context, state) {
          if (isActive) {
            if (state.stateStatus == OneStateStatus.failure) {
              OneHelperMessage.showSnackBar(context, message: state.message!);
            } else if (state.stateStatus == OneStateStatus.success) {
              /// CODE
            }
          }
        },
        builder: (context, state) {
          if (state.stateStatus == OneStateStatus.loading) {
            return OneLoadingWidget();
          }
          return Container();
        },
      ),
    );
  }
}
```

## Running Tests 🧪

To run all unit and widget tests use the following command:

```sh
$ flutter test --coverage --test-randomize-ordering-seed random
```

To view the generated coverage report you can use [lcov](https://github.com/linux-test-project/lcov)
.

```sh
# Generate Coverage Report
$ genhtml coverage/lcov.info -o coverage/

# Open Coverage Report
$ open coverage/index.html
```

---

## Working with Translations 🌐

This project relies on [flutter_localizations][flutter_localizations_link] and follows
the [official internationalization guide for Flutter][internationalization_link].

### Adding Strings

1. To add a new localizable string, open the `app_en.arb` file at `lib/l10n/arb/app_en.arb`.

```arb
{
    "@@locale": "en",
    "counterAppBarTitle": "Counter",
    "@counterAppBarTitle": {
        "description": "Text shown in the AppBar of the Counter Page"
    }
}
```

2. Then add a new key/value and description

```arb
{
    "@@locale": "en",
    "counterAppBarTitle": "Counter",
    "@counterAppBarTitle": {
        "description": "Text shown in the AppBar of the Counter Page"
    },
    "helloWorld": "Hello World",
    "@helloWorld": {
        "description": "Hello World Text"
    }
}
```

3. Use the new string

```dart
import 'package:tech/l10n/l10n.dart';

@override
Widget build(BuildContext context) {
  final l10n = context.l10n;
  return Text(l10n.helloWorld);
}
```

### Adding Supported Locales

Update the `CFBundleLocalizations` array in the `Info.plist` at `ios/Runner/Info.plist` to include
the new locale.

```xml
    ...

<key>CFBundleLocalizations</key>    <array>
<string>en</string>
<string>es</string>
</array>

    ...
```

### Adding Translations

1. For each supported locale, add a new ARB file in `lib/l10n/arb`.

```
├── l10n
│   ├── arb
│   │   ├── app_en.arb
│   │   └── app_es.arb
```

2. Add the translated strings to each `.arb` file:

`app_en.arb`

```arb
{
    "@@locale": "en",
    "counterAppBarTitle": "Counter",
    "@counterAppBarTitle": {
        "description": "Text shown in the AppBar of the Counter Page"
    }
}
```

`app_es.arb`

```arb
{
    "@@locale": "es",
    "counterAppBarTitle": "Contador",
    "@counterAppBarTitle": {
        "description": "Texto mostrado en la AppBar de la página del contador"
    }
}
```

[coverage_badge]: coverage_badge.svg

[flutter_localizations_link]: https://api.flutter.dev/flutter/flutter_localizations/flutter_localizations-library.html

[internationalization_link]: https://flutter.dev/docs/development/accessibility-and-localization/internationalization

[license_badge]: https://img.shields.io/badge/license-MIT-blue.svg

[license_link]: https://opensource.org/licenses/MIT

[very_good_analysis_badge]: https://img.shields.io/badge/style-very_good_analysis-B22C89.svg

[very_good_analysis_link]: https://pub.dev/packages/very_good_analysis

[very_good_cli_link]: https://github.com/VeryGoodOpenSource/very_good_cli
