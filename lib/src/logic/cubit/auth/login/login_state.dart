part of 'login_cubit.dart';

class LoginState {
  LoginState({
    this.stateStatus: OneStateStatus.none,
    this.message,
    this.data,
    this.id,
  });

  final OneStateStatus stateStatus;
  final String? message;
  final LoginResModel? data;
  final String? id;

  LoginState copyWith({
    OneStateStatus? stateStatus,
    String? message,
    LoginResModel? data,
    String? id,
  }) {
    return LoginState(
      stateStatus: stateStatus ?? this.stateStatus,
      message: message ?? this.message,
      data: data ?? this.data,
      id: id ?? this.id,
    );
  }
}
