import 'package:get_it/get_it.dart';
import 'package:tech/index.dart';

class AppService {
  static final _getIt = GetIt.instance;

  static GetIt get instance => _getIt;

  static GetIt get I => _getIt;

  static bool get isRegistered => _isRegistered;
  static bool _isRegistered = false;

  static final AppService _instance = AppService._internal();

  factory AppService() => _instance;

  AppService._internal();

  Future<void> init() async {
    try {
      _getIt.registerLazySingleton<AuthService>(() => AuthService());
      _getIt.registerLazySingleton<DeviceTokenService>(() => DeviceTokenService());
      _getIt.registerLazySingleton<FirebaseMessagingService>(() => FirebaseMessagingService());
      _getIt.registerLazySingleton<LocalNotification>(() => LocalNotification());

      // _getIt.registerSingletonAsync<SettingCubit>(() async => await SettingCubit()
    } catch (_) {}

    /// IS REGISTERED =====
    _isRegistered = true;
  }
}
