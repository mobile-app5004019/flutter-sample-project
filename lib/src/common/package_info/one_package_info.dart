import 'package:package_info_plus/package_info_plus.dart';

class OnePackageInfo {
  static final OnePackageInfo instance = OnePackageInfo._init();

  static PackageInfo? _packageInfo;

  OnePackageInfo._init();

  static Future init() async {
    if (_packageInfo != null) return;
    _packageInfo = await PackageInfo.fromPlatform();
    return _packageInfo;
  }

  static PackageInfo get value => _packageInfo!;

  static Future<String> get appName async => _packageInfo!.appName;

  static Future<String> get packageName async => _packageInfo!.packageName;

  static String get version {
    final versionSplit = value.version.split('-');
    return versionSplit[0];
  }

  static Future<String> get buildNumber async => _packageInfo!.buildNumber;
}
