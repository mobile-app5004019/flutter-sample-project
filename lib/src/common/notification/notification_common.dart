import 'dart:convert';
import 'package:tech/index.dart';

enum EnumNotificationType {
  topic_allUser,
}

enum EnumNotificationActionType {
  onMessagePageItemPressed,
  onBannerStatusBarPressed,
  onLocalBannerStatusBarPressed,
  onOpenedAppPushed,
}

class CommonNotification {
  static List<Map<EnumNotificationType, String>> type = [
    {EnumNotificationType.topic_allUser: 'allUser'},
  ];

  static EnumNotificationType? getTypeByString(String? value) {
    if (value == null) return null;
    final int result = type.indexWhere((Map<EnumNotificationType, String> element) => element.values.first == value);
    if (result > -1) {
      return type[result].keys.first;
    }
    return null;
  }

  static String? getStringByType(EnumNotificationType? value) {
    if (value == null) return null;
    final int result = type.indexWhere((element) => element.keys.first == value);
    if (result > -1) {
      return type[result].values.first;
    }
    return null;
  }

  /// EnumNotificationType.topic_allUser: 'allUser
  static void allUser(
    context,
    Map<String, dynamic> data, {
    EnumNotificationActionType actionType: EnumNotificationActionType.onOpenedAppPushed,
    required String title,
    required String body,
  }) {
    try {
      EnumNotificationType type = EnumNotificationType.topic_allUser;
      if (actionType == EnumNotificationActionType.onBannerStatusBarPressed || actionType == EnumNotificationActionType.onMessagePageItemPressed) {
        try {
          /// code
        } catch (_) {}
        return;
      }
      return;
    } catch (_) {}
  }
}
