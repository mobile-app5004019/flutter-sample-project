import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:tech/index.dart';

///ConnectivityUtil
class ConnectivityUtil {
  static Connectivity? _connectInstance;

  static Future<Connectivity> get _instance async {
    if (_connectInstance == null) _connectInstance = await Connectivity();
    return _connectInstance!;
  }

  ///
  static Future<bool> init() async {
    _connectInstance = await _instance;
    _connectInstance!.onConnectivityChanged.listen(_onConnectionChanged);
    return true;
  }

  static ConnectivityResult? _previousConnectivityResult;

  static void _onConnectionChanged(connectionResult) {
    if (connectionResult != ConnectivityResult.none && _previousConnectivityResult == ConnectivityResult.none) {
      oneEventBus.fire(OneEventBusConnectivity(isReconnected: true));
    } else {
      oneEventBus.fire(OneEventBusConnectivity(isReconnected: false));
    }
    _previousConnectivityResult = connectionResult;
  }
}
