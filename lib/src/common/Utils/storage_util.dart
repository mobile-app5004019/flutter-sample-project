import 'package:shared_preferences/shared_preferences.dart';

class StorageUtil {
  static SharedPreferences? _prefsInstance;

  static Future<SharedPreferences> get _instance async {
    if (_prefsInstance == null) _prefsInstance = await SharedPreferences.getInstance();
    return _prefsInstance!;
  }

  // call this method from iniState() function of mainApp().
  static Future<bool> init() async {
    _prefsInstance = await _instance;
    return true;
  }

  static String getString(String key, [String? defaultValue]) {
    if (_prefsInstance == null) init();
    return _prefsInstance?.getString(key) ?? defaultValue ?? '';
  }

  static Future<bool> setString(String key, String value) async {
    var prefs = await _instance;
    return prefs.setString(key, value);
  }

  static double? getDouble(String key, [double? defaultValue]) {
    if (_prefsInstance == null) init();
    return _prefsInstance?.getDouble(key) ?? defaultValue ?? 0.0;
  }

  static Future<bool> setDouble(String key, double value) async {
    var prefs = await _instance;
    return prefs.setDouble(key, value);
  }

  static Future<bool> setBool(String key, bool? value) async {
    var prefs = await _instance;
    return prefs.setBool(key, value ?? false);
  }

  static bool getBool(String key, [bool? defaultValue]) {
    if (_prefsInstance == null) init();
    return _prefsInstance?.getBool(key) ?? (defaultValue ?? false);
  }

  static Future<bool>? remove(String key) {
    return _prefsInstance?.remove(key);
  }
}
