import 'dart:async';
import 'dart:io';
import 'package:tech/index.dart';

class _OneHttpModel {
  final String url;
  final int? timeOut;
  final Map<String, dynamic>? fields;
  final Map<String, List<dynamic>>? fieldsList;
  final Map<String, File>? files;
  final Map<String, String>? headers;
  final Map<String, List<File>>? filesList;

  _OneHttpModel({
    this.url = '',
    this.timeOut,
    this.fields,
    this.fieldsList,
    this.files,
    this.filesList,
    this.headers,
  });
}

class OneHttp {
  final String url;
  final int? timeOut;
  final Map<String, dynamic>? fields;
  final Map<String, List<dynamic>>? fieldsList;
  final Map<String, File>? files;
  final Map<String, String>? headers;
  final Map<String, List<File>>? filesList;

  final bool addBaseUrl;
  final bool addDefaultHeader;
  final bool addToken;
  bool _isRepeat = false;

  OneHttp({
    this.url = '',
    this.timeOut,
    this.fields,
    this.fieldsList,
    this.files,
    this.filesList,
    this.headers,
    this.addBaseUrl = true,
    this.addDefaultHeader = true,
    this.addToken = true,
  }) {}

  Future<Map<String, dynamic>> get({String? newToken}) async {
    if (this.url == '') {
      return Future.error(oneG.context.l10n.somethingUnexpectedWentWrong);
    }
    _OneHttpModel _httpModel = _init(newToken: newToken);
    return OnePKHttp.get(
      url: _httpModel.url,
      fields: _httpModel.fields,
      headers: _httpModel.headers,
      timeOut: _httpModel.timeOut,
    ).then((value) {
      return value;
    }).onError((error, stackTrace) {
      return _checkWhenError(error, OneHttpType.get);
    });
  }

  Future<Map<String, dynamic>> post({String? newToken}) async {
    if (this.url == '') {
      return Future.error(oneG.context.l10n.somethingUnexpectedWentWrong);
    }
    _OneHttpModel _httpModel = _init(newToken: newToken);
    return OnePKHttp.post(
      url: _httpModel.url,
      fields: _httpModel.fields,
      fieldsList: _httpModel.fieldsList,
      files: _httpModel.files,
      filesList: _httpModel.filesList,
      headers: _httpModel.headers,
      timeOut: _httpModel.timeOut,
    ).then((value) {
      return value;
    }).onError((error, stackTrace) {
      return _checkWhenError(error, OneHttpType.post);
    });
  }

  _OneHttpModel _init({String? newToken}) {
    Map<String, String> _headers = {};
    String _url = '';
    Map<String, dynamic>? _fields = {};
    Map<String, List<dynamic>>? _fieldsList = {};
    Map<String, List<File>>? _filesList = {};
    if (this.addDefaultHeader) {
      _headers.addAll(OneVariable.defaultHeader);
    }
    if (this.headers != null) {
      _headers.addAll(this.headers!);
    }
    if (this.addBaseUrl) {
      _url = OneVariable.baseUrl;
    }
    _url = _url + this.url;
    if (this.addToken) {
      final _userToken = StorageUtil.getString(ACCESS_TOKEN);
      // Map<String, dynamic>? token = {
      //   "Authorization": "Bearer $_userToken",
      //   "token": _userToken,
      // };
      if (_userToken != "") {
        _headers['Authorization'] = "Bearer $_userToken";
      }
      //_headers['token'] = _userToken;
      //_fields.addAll(token);
    }
    if (this.fields != null) {
      _fields.addAll(this.fields!);
    }
    if (this.fieldsList != null) {
      _fieldsList.addAll(this.fieldsList!);
    }
    if (this.filesList != null) {
      _filesList.addAll(this.filesList!);
    }
    if (newToken != null) {
      _headers['Authorization'] = "Bearer $newToken";
      //_headers['token'] = newToken;
    }
    /*if (newToken != null && checkKeyMap(_fields, 'Authorization')) {
      _fields['token'] = newToken;
    }*/
    return new _OneHttpModel(
      url: _url,
      fields: _fields,
      files: this.files,
      fieldsList: _fieldsList,
      filesList: _filesList,
      headers: _headers,
      timeOut: this.timeOut,
    );
  }

  Future<Map<String, dynamic>> _checkWhenError(dynamic error, OneHttpType httpType) async {
    if (error is OneHttpError) {
      if (error.status == OneHttpStatus.tokenExpired) {
        return _checkToken(error, OneHttpType.post);
      }
      return Future.error(error.message);
    } else {
      return Future.error(oneG.context.l10n.somethingUnexpectedWentWrong);
    }
  }

  Future<Map<String, dynamic>> _checkToken(dynamic error, OneHttpType httpType) async {
    if (error is OneHttpError) {
      if (this._isRepeat == true) {
        return Future.error(error.message);
      }
      this._isRepeat = true;
      final refresh = StorageUtil.getString(REFRESH_TOKEN);
      return Future.error(error);
      /*return OnePKHttp.post(
        url: OneVariable.baseUrl + OnePath.REFRESH_TOKEN,
        headers: OneVariable.defaultHeader,
        fields: {"refresh_token": refresh},
      ).then((value) {
        var response = OneResponse<RefreshTokenResModel>.fromJson(value, (data) => RefreshTokenResModel.fromJson(data));
        StorageUtil.setString(REFRESH_TOKEN, response.data!.refreshToken);
        StorageUtil.setString(ACCESS_TOKEN, response.data!.token);
        if (httpType == OneHttpType.post) {
          return this.post(newToken: response.data!.token);
        }
        return this.get(newToken: response.data!.token);
      }).onError((error, stackTrace) {
        if (error is OneHttpError) {
          if (error.status == OneHttpStatus.refreshTokenExpired) {
            oneEventBus.fire(OneForeLogout(true));
          }
          return Future.error(error.message);
        } else {
          return Future.error(error == null ? oneG.context.l10n.somethingUnexpectedWentWrong : error);
        }
      });*/
    } else {
      return Future.error(error);
    }
  }
}

class OneForeLogout {
  final bool isLogout;

  OneForeLogout(this.isLogout);
}
