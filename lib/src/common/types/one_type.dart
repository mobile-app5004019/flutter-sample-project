import 'package:dartz/dartz.dart';
import 'package:tech/index.dart';

typedef OneResponseEither<Type> = Future<Either<String, Type>>;
typedef OneJson = Map<String, dynamic>;

class OneResponse<T extends Serializable> {
  int code;
  bool status;
  String message;
  T? data;
  List<T>? dataList;

  OneResponse({
    required this.code,
    required this.status,
    required this.message,
    required this.data,
    required this.dataList,
  });

  factory OneResponse.fromJson(Map<String, dynamic> json, Function(Map<String, dynamic>) create) {
    try {
      final List<T>? itemList = json["data"] == null || json["data"] is Map ? null : List<T>.from(json["data"].map((x) => create(x)));
      final T? itemMap = json["data"] == null || json["data"] is List ? null : create(json["data"]);
      return OneResponse<T>(
        code: json["code"].toString().toOneInt(),
        status: json["status"],
        message: json["message"].toString().toOneString()!,
        data: itemMap,
        dataList: itemList == null ? [] : itemList,
      );
    } catch (e) {
      print(e);
      throw oneG.context.l10n.somethingUnexpectedWentWrong;
    }
  }

  Map<String, dynamic> toJson() => {
        "code": this.code,
        "status": this.status,
        "message": this.message,
        "data": this.data?.toJson(),
        "dataList": this.dataList?.map((x) => x.toJson()).toList(),
      };
}

abstract class Serializable {
  Map<String, dynamic> toJson();
}

class OneResponsePaginateDataPaginate {
  OneResponsePaginateDataPaginate({
    required this.total,
    required this.perPage,
    required this.currentPage,
    required this.nextPageUrl,
  });

  final String total;
  final String perPage;
  final String currentPage;
  final bool nextPageUrl;

  factory OneResponsePaginateDataPaginate.fromJson(Map<String, dynamic> json) {
    try {
      return OneResponsePaginateDataPaginate(
        total: json["total"].toString().toOneString()!,
        perPage: json["per_page"].toString().toOneString()!,
        currentPage: json["current_page"].toString().toOneString()!,
        nextPageUrl: json["next_page_url"],
      );
    } catch (e) {
      print(e);
      throw oneG.context.l10n.somethingUnexpectedWentWrong;
    }
  }

  Map<String, dynamic> toJson() => {
        "total": total,
        "per_page": perPage,
        "current_page": currentPage,
        "next_page_url": nextPageUrl,
      };
}

class OneResponsePaginateData<T> {
  List<T>? items;
  T? itemMap;
  OneResponsePaginateDataPaginate paginate;

  OneResponsePaginateData({
    required this.items,
    required this.itemMap,
    required this.paginate,
  });

  factory OneResponsePaginateData.fromJson(Map<String, dynamic> json, List<T>? t, T? tMap) {
    try {
      return OneResponsePaginateData(
        paginate: OneResponsePaginateDataPaginate.fromJson(json['paginate']),
        items: t == null ? [] : t,
        itemMap: tMap,
      );
    } catch (e) {
      print(e);
      throw oneG.context.l10n.somethingUnexpectedWentWrong;
    }
  }

  Map<String, dynamic> toJson() => {};
}

class OneResponsePaginate<T extends SerializablePaginate> {
  int code;
  bool status;
  String message;
  OneResponsePaginateData<T> data;

  OneResponsePaginate({
    required this.code,
    required this.status,
    required this.message,
    required this.data,
  });

  factory OneResponsePaginate.fromJson(Map<String, dynamic> json, Function(Map<String, dynamic>) create) {
    try {
      final _json = checkKeyMap(json["data"], 'item') ? json["data"]["item"] : json["data"]["items"];
      final List<T>? itemList = _json != null && _json is List ? List<T>.from(_json.map((x) => create(x))) : null;
      final T? itemMap = _json != null && _json is Map ? create(_json as Map<String, dynamic>) : null;
      print("data");
      return OneResponsePaginate<T>(
        code: json["code"].toString().toOneInt(),
        status: json["status"],
        message: json["message"].toString().toOneString()!,
        data: OneResponsePaginateData<T>.fromJson(json["data"], itemList, itemMap),
      );
    } catch (e) {
      return OneResponsePaginate<T>(
        code: 200,
        status: true,
        message: "",
        data: OneResponsePaginateData<T>(
          items: [],
          itemMap: null,
          paginate: OneResponsePaginateDataPaginate(
            total: '0',
            perPage: '0',
            currentPage: '0',
            nextPageUrl: false,
          ),
        ),
      );
    }
  }

  Map<String, dynamic> toJson() => {
        "code": this.code,
        "status": this.status,
        "message": this.message,
        "data": this.data.toJson(),
      };
}

abstract class SerializablePaginate {
  Map<String, dynamic> toJson();
}
