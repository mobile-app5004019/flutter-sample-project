import 'dart:io';
import 'package:tech/index.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';

/// Three type of environment
const String PRODUCTION_LABEL_NAME = "Production";
const String DEVELOPMENT_LABEL_NAME = "Dev";
const String STAGING_LABEL_NAME = "Staging";

/// Key variable
const String BASE_URL_DEFAULT = "baseUrlDefault";
const String BASE_URL = "baseUrl";
const String BASE_PHOTO_URL = "basePhotoUrl";
const String SECRET_KEY = "secretKey";
const String BASE_URL_COPY_LINK = "baseUrlCopyLink";

/// Backend Security
const String API_SECURITY = "Authorization";

/// API Version
const String API_VERSION = "v2";

/// Production Environment
const Map<String, dynamic> productionEnvironment = {
  BASE_URL_DEFAULT: "https://api.supperlive.com",
  BASE_URL: "https://api.supperlive.com/api/mobile/",
  BASE_PHOTO_URL: "https://s3.supperlive.com/supperlive/",
  SECRET_KEY: "3qGXP86Wgqpsu5hk9JTn9Egc8Qq5OzXx",
  BASE_URL_COPY_LINK: "https://supperlive.com/",
  API_SECURITY: {
    API_SECURITY: "base64:RZhlxrPfgeyGRjC//bJxV/Pmq9sI4SQYhJc0H9vvyIg=",
  }
};

/// Development Environment
const Map<String, dynamic> developmentEnvironment = {
  BASE_URL_DEFAULT: "https://dev-api.supperlive.com",
  BASE_URL: "https://dev-api.supperlive.com/api/mobile/",
  BASE_PHOTO_URL: "https://s3.supperlive.com/supperlive-dev/",
  SECRET_KEY: "3qGXP86Wgqpsu5hk9JTn9Egc8Qq5OzXx",
  BASE_URL_COPY_LINK: "https://dev-front.supperlive.com/",
  API_SECURITY: {
    API_SECURITY: "base64:RZhlxrPfgeyGRjC//bJxV/Pmq9sI4SQYhJc0H9vvyIg=",
  }
};

/// Staging Environment
const Map<String, dynamic> stagingEnvironment = {
  // BASE_URL_DEFAULT: "http://192.168.0.208:8000",
  BASE_URL_DEFAULT: "https://dev-api.supperlive.com",
  // BASE_URL: "http://192.168.0.208:8000/api/mobile/",
  BASE_URL: "https://dev-api.supperlive.com/api/mobile/",
  BASE_PHOTO_URL: "https://s3.supperlive.com/supperlive-dev/",
  SECRET_KEY: "3qGXP86Wgqpsu5hk9JTn9Egc8Qq5OzXx",
  BASE_URL_COPY_LINK: "https://dev-front.supperlive.com/",
  API_SECURITY: {
    API_SECURITY: "base64:RZhlxrPfgeyGRjC//bJxV/Pmq9sI4SQYhJc0H9vvyIg=",
  }
};

class OneVariable {
  static const bool isShowAds = false; // true
  static const bool isDebug = false; // false
  static const int pageSize = 12;
  static const int fileUploadMaxSize = 10;

  static String baseUrlDefault = FlavorConfig.instance!.variables[BASE_URL_DEFAULT];
  static String baseUrl = FlavorConfig.instance!.variables[BASE_URL] + API_VERSION + "/";
  static String baseUrl_V1 = FlavorConfig.instance!.variables[BASE_URL] + "v1/";
  static String basePhotoUrl = FlavorConfig.instance!.variables[BASE_PHOTO_URL];
  static String secretKey = FlavorConfig.instance!.variables[SECRET_KEY];
  static String baseUrlCopyLink = FlavorConfig.instance!.variables[BASE_URL_COPY_LINK];

  static Map<String, String> get defaultHeader {
    Map<String, String> header = {};
    header.addAll({'Content-Type': "application/json"});
    header.addAll({'Accept': "application/json"});
    header.addAll({'lang': StorageUtil.getString(LANGUAGE_KEY)});
    header.addAll({'platform': Platform.operatingSystem});
    header.addAll({'is_mobile': "1"});
    header.addAll({'api_version': API_VERSION});
    header.addAll({'app_version': OnePackageInfo.version});
    header.addAll({'Connection': "keep-alive"});
    return header;
  }
}
