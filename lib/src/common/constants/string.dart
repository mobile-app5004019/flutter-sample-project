/// SharedPreferences String key
const String FIRST_TIME_OPENED_APP = 'FIRST_TIME_OPENED_APP';
const String THEME_MODE = 'THEME_MODE';
const String IS_NOTIFICATION_ON = 'IS_NOTIFICATION_ON'; // bool
const String ACCESS_TOKEN = 'ACCESS_TOKEN';
const String REFRESH_TOKEN = 'REFRESH_TOKEN';
const String USER_DATA = 'USER_DATA';
const String LANGUAGE_KEY = 'LANGUAGE_KEY';
const String HOME_SEARCH_HISTORY = 'HOME_SEARCH_HISTORY';
const String EVENT_SEARCH_HISTORY = 'EVENT_SEARCH_HISTORY';
const String SCHOLARSHIP_SEARCH_HISTORY = 'SCHOLARSHIP_SEARCH_HISTORY';
const String CAREER_SEARCH_HISTORY = 'CAREER_SEARCH_HISTORY';
const String CARD_SEARCH_HISTORY = 'CARD_SEARCH_HISTORY';

/// Mock Data
const String IMG_Mock =
    "https://leverageedu.com/blog/wp-content/uploads/2019/08/Course-after-MBA.png";
const String IMAGE_HASH = "LNEyuVxcE1x?~X%fRiMy9ZMwVtNx";

/// use for test long text
const String LOREM_TEXT = """
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
    tempor incididunt ut labore et dolore magna aliqua. Egestas egestas 
    fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate. 
    Feugiat in fermentum posuere urna. Amet nisl purus in mollis nunc. 
    Tellus orci ac auctor augue mauris augue. Dolor morbi non arcu risus 
    quis varius quam quisque id. Et malesuada fames ac turpis egestas 
    sed tempus. Eget mi proin sed libero enim sed faucibus. Turpis massa 
    sed elementum tempus. Congue eu consequat ac felis donec
    """;

const String TOKEN_MOCK =
    "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJDNEtzLUJMYm9KM25pY0hJNUpzNzQtS0hJcTZjcWdvLWtMNmdMWndJdWxvIn0.eyJleHAiOjE2MjcxMDgwODEsImlhdCI6MTYyNjQxNjg4MSwianRpIjoiZTc0ZDIwMzEtMWI4OS00Yjc5LThmMTUtOTZiZTNhNTVkZGQ4IiwiaXNzIjoiaHR0cDovL2lkcy5vbmVzYWxhLmNvbTo4MDgwL2F1dGgvcmVhbG1zL29uZXNhbGEiLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiZjoxNDVlODJlNS1lOTQzLTQ0YmItYmM3Ni0yNDIwMGIyYzM4MjA6MTA2MTM1MTUiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJvbmVzYWxhLWFwcCIsInNlc3Npb25fc3RhdGUiOiIzOWY4OGNmYS01NjZhLTQ5MTMtOWU2Ny0yYTM1M2NkZjUyZDYiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHBzOi8va2V5Y2xvYWs0LnRlc3QiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6Im9wZW5pZCBlbWFpbCBwaG9uZV9udW1iZXIgcHJvZmlsZSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwicHJlZmVycmVkX3VzZXJuYW1lIjoiMTA2MTM1MTUifQ.a-N-XKAOnxkCRNvxdOk5rVidoQzLWNX5faGPK2MT4MWnVV1R5BeX2XYTl2Iv8I69BJezoOumVFrgBXHnKiwWhFofZttMF3h8CVnRmzLMvfl1oETj4aQIYeBNShTKtYARjPjcxTiVrk97lhInnTa71xwDS5QsEJL9zTjEipZQDGjMctPPhigqSoEuBBBOGsOVOsF6SoSKCHjmuAyqApFv_fYttUxdQ7p0k8TUbFLwE4SM4OmmLhe1rgAT9FrKuvk3HN9uYJwTw-1gDx_eM4xucVXXCWIiv-8nOzFXXWxK7v2AkDAhXdN65vP_Q1i3wBTcF0OCbPSzYEPZCtBoaH1_Dg";
