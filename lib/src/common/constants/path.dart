class OnePath {
  static const String LOGIN = 'login';
  static const String LOGOUT = 'logout';
  static const String REGISTER = 'account/register';

  static const String CHECK_VERSION_URL = 'app/version';

  static const String COUNTRY = 'system/country';
  static const String PROVINCE = 'system/country/province';
  static const String DISTRICT = 'system/province/district';
  static const String GET_IP_DETAILS = 'get-ip-details';
  static const String PRIVACY = 'page/privacy';
  static const String ABOUT = 'page/about';
  static const String SAVE_DEVICE_ID = 'device-id';
  static const String DELETE_DEVICE_ID = 'device-id/delete';
}
