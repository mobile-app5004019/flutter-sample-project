import 'package:tech/index.dart';

class CustomColor {
  static const Color green = Color(0xFF39B54A);
  static const Color textGreen = Color(0xFF009D4C);
  static const Color lightGreen = Color(0xFFCEFFBA);
  static const Color black = Color(0xFF212121);
  static const Color white = Color(0xFFFFFFFF);
  static const Color gray = Color(0xFF696969);

  static const Color whiteGray = Color(0xFFEFEFEF);
  //static const Color red = Color(0xFFDD0426);
  static const Color red = Color(0xFFDD0426);
  static const int primaryHex = 0xFFFF6900;
  static const Color primary = Color(primaryHex);
  static const Color lightGray = Color(0xFF969696);
  static const Color remainItemColor = Color(0xFFFAFAFA);
  static const Color bgColor = Colors.white;
  static const Color bgGreyColor = Color(0xFFF5F5F5);
  static const Color disableColor = Color(0xffFFEA9F);
  static const Color disableTextFieldColor = Color(0xffFAFAFA);

  static const Color textFeeColor = Color(0xff009D4C);

  static const Color yellow = Color(0xFFFFC800);
  static const Color blue = Color(0xFF486AF1);
}

class OneColors {
  static Color like(BuildContext context) {
    return Color(0xFFDE3D35);
  }

  static Color grayLight(BuildContext context) {
    return !isThemeDark(context) ? Colors.grey[500]! : Colors.grey[500]!;
  }

  static Color grayLightWhite(BuildContext context) {
    return !isThemeDark(context) ? Color(0xFFEFEFEF) : Colors.grey[400]!;
  }

  static Color grayLightSky(BuildContext context) {
    return !isThemeDark(context) ? Color(0xFFF3F6F9) : Colors.grey[800]!;
  }

  static Color graySkyDark(BuildContext context) {
    return Color(0xFF1F78B4);
  }

  static Color grayDark(BuildContext context, {bool isBgGrey = false}) {
    return !isThemeDark(context) ? Color(0xFF696969) : (!isBgGrey ? Colors.grey[600]! : Colors.grey[400]!);
  }

  static Color grayDarkMore(BuildContext context, {bool isBgGrey = false}) {
    return !isThemeDark(context) ? Color(0xFF9CA3AF) : (!isBgGrey ? Colors.grey[700]! : Colors.grey[400]!);
  }

  static Color gray(BuildContext context, {bool isBgGrey = false}) {
    return !isThemeDark(context) ? Color(0xFFD0D3D9) : (!isBgGrey ? Colors.grey[700]! : Colors.grey[400]!);
  }

  static Color divider(BuildContext context) {
    return !isThemeDark(context) ? Color(0xFFD1D5DB) : Colors.grey[850]!;
  }

  static Color active(BuildContext context) {
    return !isThemeDark(context) ? Color(0xFF4B5563) : oneIconColor(context);
  }

  static Color deActive(BuildContext context) {
    return !isThemeDark(context) ? Color(0xFFD1D5DB) : Colors.grey[700]!;
  }

  static Color borderLight(BuildContext context) {
    return !isThemeDark(context) ? Color(0xFFE2E8F0) : Colors.grey[800]!;
  }
}
