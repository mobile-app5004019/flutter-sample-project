import 'package:flutter/services.dart';
import 'package:get/get_navigation/src/routes/transitions_type.dart';
import 'package:tech/index.dart';
import 'package:tech/src/bloc_providers.dart';
import 'package:tech/src/view/routes/page_routes.dart';
export 'route_active_mixin.dart';

class App extends StatefulWidget {
  App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
  static final RouteObserver<Route<dynamic>> routeObserver = RouteObserver<Route<dynamic>>();
}

class _AppState extends State<App> {
  String get _getLangLocalStorage => StorageUtil.getString(LANGUAGE_KEY);

  String get _getThemeModeLocalStorage => StorageUtil.getString(THEME_MODE);

  Locale get _getLocal => _getLangLocalStorage == "" ? const Locale('en', 'EN') : (_getLangLocalStorage == "km" ? const Locale('km', 'KM') : const Locale('en', 'EN'));

  ThemeMode get _getThemeMode => _getThemeModeLocalStorage == "" || _getThemeModeLocalStorage == OneThemeMode.system.name ? oneGetMode(OneThemeMode.system) : (_getThemeModeLocalStorage == OneThemeMode.dark.name ? ThemeMode.dark : ThemeMode.light);

  @override
  void initState() {
    // TODO: implement initState
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    SystemChrome.setPreferredOrientations(
      [
        DeviceOrientation.portraitUp,
      ],
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FlavorBanner(
      child: MultiBlocProvider(
        providers: BlocProviders.providers,
        child: GetMaterialApp(
          title: 'Superlive',
          initialRoute: SplashScreenPage.routeName,
          theme: drLightTheme,
          //darkTheme: oneDarkTheme,
          scrollBehavior: _ScrollBehavior(),
          themeMode: ThemeMode.light,
          // themeMode: _getThemeMode,
          locale: _getLocal,
          getPages: getPages,
          navigatorKey: oneG.navigatorKey,
          navigatorObservers: <NavigatorObserver>[
            App.routeObserver,
          ],
          localizationsDelegates: AppLocalizations.localizationsDelegates,
          supportedLocales: AppLocalizations.supportedLocales,
          defaultTransition: Transition.cupertino,
        ),
      ),
    );
  }
}

class _ScrollBehavior extends ScrollBehavior {
  const _ScrollBehavior();

  @override
  ScrollPhysics getScrollPhysics(BuildContext context) {
    switch (getPlatform(context)) {
      case TargetPlatform.iOS:
      case TargetPlatform.macOS:
      case TargetPlatform.android:
        return const BouncingScrollPhysics();
      case TargetPlatform.fuchsia:
      case TargetPlatform.linux:
      case TargetPlatform.windows:
        return const ClampingScrollPhysics();
    }
  }
}
