import 'dart:convert';
import 'package:tech/index.dart';

class LoginResModel implements Serializable {
  late UserDataModel items;
  late String token;
  late String refreshToken;

  LoginResModel({
    required this.items,
    required this.token,
    required this.refreshToken,
  });

  factory LoginResModel.fromJson(Map<String, dynamic> json) {
    try {
      return LoginResModel(
        items: UserDataModel.fromJson(json["items"]),
        token: json["token"].toString().toOneString()!,
        refreshToken: json["refresh_token"].toString().toOneString()!,
      );
    } catch (e) {
      throw e;
    }
  }

  @override
  Map<String, dynamic> toJson() => {
        "item": items.toJson(),
        "token": token,
        "refresh_token": refreshToken,
      };
}

class UserDataModel extends Serializable {
  final String id;
  final String lastName;
  final String firstName;
  final String userName;
  final String pictureFileName;
  final String pictureFolder;
  final String headline;
  final String gender;
  final String dob;
  final String countryId;
  final String address;
  final String provinceId;
  final String email;
  final String phoneNumber;
  final bool isCompletProfile;

  UserDataModel({
    required this.id,
    required this.lastName,
    required this.firstName,
    required this.userName,
    required this.pictureFileName,
    required this.pictureFolder,
    required this.headline,
    required this.gender,
    required this.dob,
    required this.countryId,
    required this.address,
    required this.provinceId,
    required this.email,
    required this.phoneNumber,
    required this.isCompletProfile,
  });

  factory UserDataModel.fromJson(Map<String, dynamic> json) => UserDataModel(
        id: json["id"].toString().toOneString()!,
        lastName: json["last_name"].toString().toOneString()!,
        firstName: json["first_name"].toString().toOneString()!,
        userName: json["user_name"].toString().toOneString()!,
        pictureFileName: json["picture_file_name"].toString().toOneString()!,
        pictureFolder: json["picture_folder"].toString().toOneString()!,
        headline: json["headline"].toString().toOneString()!,
        gender: json["gender"].toString().toOneString()!,
        dob: json["dob"].toString().toOneString()!,
        countryId: json["country_id"].toString().toOneString()!,
        address: json["address"].toString().toOneString()!,
        provinceId: json["province_id"].toString().toOneString()!,
        email: json["email"].toString().toOneString()!,
        phoneNumber: json["phone_number"].toString().toOneString()!,
        isCompletProfile: json["is_complet_profile"] == 1,
      );

  UserDataModel copyWith({
    String? id,
    String? lastName,
    String? firstName,
    String? userName,
    String? pictureFileName,
    String? pictureFolder,
    String? headline,
    String? gender,
    String? dob,
    String? countryId,
    String? address,
    String? provinceId,
    String? email,
    String? phoneNumber,
    bool? isCompletProfile,
  }) {
    return UserDataModel(
      id: id ?? this.id,
      lastName: lastName ?? this.lastName,
      firstName: firstName ?? this.firstName,
      userName: userName ?? this.userName,
      pictureFileName: pictureFileName ?? this.pictureFileName,
      pictureFolder: pictureFolder ?? this.pictureFolder,
      headline: headline ?? this.headline,
      gender: gender ?? this.gender,
      dob: dob ?? this.dob,
      countryId: countryId ?? this.countryId,
      address: address ?? this.address,
      provinceId: provinceId ?? this.provinceId,
      email: email ?? this.email,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      isCompletProfile: isCompletProfile ?? this.isCompletProfile,
    );
  }

  Map<String, dynamic> toJson() => {
        'id': this.id,
        'last_name': this.lastName,
        'first_name': this.firstName,
        'user_name': this.userName,
        'picture_file_name': this.pictureFileName,
        'picture_folder': this.pictureFolder,
        'headline': this.headline,
        'gender': this.gender,
        'dob': this.dob,
        'country_id': this.countryId,
        'address': this.address,
        'province_id': this.provinceId,
        'email': this.email,
        'phone_number': this.phoneNumber,
        'is_complet_profile': this.isCompletProfile ? 1 : 0,
      };
}
