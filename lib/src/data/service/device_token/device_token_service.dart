import 'package:dartz/dartz.dart';
import 'package:tech/index.dart';

class DeviceTokenService {
  OneResponseEither<bool> saveDeviceToken(String deviceToken) async {
    try {
      await OneHttp(
        url: OnePath.SAVE_DEVICE_ID,
        fields: {"device_id": deviceToken},
      ).post();
      return right(true);
    } catch (e) {
      print(e);
      return left(oneG.context.l10n.somethingUnexpectedWentWrong);
    }
  }

  OneResponseEither<bool> deleteDeviceToken(String? deviceToken) async {
    try {
      await OneHttp(
        url: OnePath.DELETE_DEVICE_ID,
        fields: {"device_id": deviceToken ?? ''},
      ).post();
      return right(true);
    } catch (e) {
      print(e);
      return left(oneG.context.l10n.somethingUnexpectedWentWrong);
    }
  }
}
