import 'package:flutter/material.dart';
import 'package:tech/index.dart';

class SettingMenuItemWidget extends StatelessWidget {
  SettingMenuItemWidget({
    super.key,
    required this.leading,
    required this.title,
    this.textColor,
    this.hasDivider: false,
    this.trailing,
    this.onTap,
  });

  final Widget leading;
  final String title;
  Widget? trailing;
  Function? onTap;
  final Color? textColor;
  final bool hasDivider;
  @override
  Widget build(BuildContext context) {
    return OneGestureDetector(
      onTap: () {
        if (onTap != null) onTap!();
      },
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 12),
            child: Row(
              children: [
                Container(
                  width: 25,
                  child: leading,
                ),
                SizedBox(width: 15),
                Expanded(
                  child: Text(
                    title,
                    style: oneTextTheme(context).displaySmall!.copyWith(
                          fontWeight: FontWeight.w500,
                          color: textColor,
                        ),
                  ),
                ),
                if (trailing != null)
                  Container(
                    margin: const EdgeInsets.only(left: 10),
                    child: trailing!,
                  ),
              ],
            ),
          ),
          if (hasDivider) Divider(height: 2),
        ],
      ),
    );
  }
}
