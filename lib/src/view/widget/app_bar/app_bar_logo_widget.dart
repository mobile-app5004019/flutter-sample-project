import 'package:tech/index.dart';

class AppBarLogoWidget extends StatelessWidget {
  const AppBarLogoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OneWidgetSvgImage(
      path: OneSvgHeader.mainLogo,
      width: 150,
    );
  }
}
