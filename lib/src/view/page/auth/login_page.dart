/// login_page
import 'package:tech/index.dart';

class LoginPage extends StatefulWidget {
  static const String routeName = '/login_page';

  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

///extends State<LoginPage>
class _LoginPageState extends State<LoginPage> with RouteActiveMixin<LoginPage> {
  late final LoginCubit _cubit;
  final usernameTextController = TextEditingController();
  final passwordTextController = TextEditingController();

  @override
  void initState() {
    _cubit = context.read<LoginCubit>();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _cubit.reset();
  }

  @override
  Widget build(BuildContext context) {
    return OneGestureDetector(
      onTap: () {
        OneHelperWidget.focusNew(context);
      },
      child: Scaffold(
        appBar: AppBar(
          leading: OneLeadingAppbar(),
          title: Text(context.l10n.login),
        ),
        body: BlocConsumer<LoginCubit, LoginState>(
          listener: (context, state) {
            if (isActive) {
              if (state.stateStatus == OneStateStatus.failure) {
                OneHelperMessage.showSnackBar(context, message: state.message!);
              } else if (state.stateStatus == OneStateStatus.success) {
                /// CODE
              }
            }
          },
          builder: (context, state) {
            if (state.stateStatus == OneStateStatus.loading) {
              return OneLoadingWidget();
            }
            return Container(
              margin: EdgeInsets.symmetric(horizontal: 16),
              child: OneListViewBuilder(
                children: [
                  OneFormTextField(
                    controller: usernameTextController,
                    text: context.l10n.phone,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  OneFormTextField(
                    controller: passwordTextController,
                    secureText: true,
                    text: context.l10n.password,
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  OneFormButton(
                    text: context.l10n.login,
                    onPressed: () {
                      _cubit.login(LoginReqModel(
                        username: usernameTextController.text,
                        password: passwordTextController.text,
                      ));
                    },
                  ),
                ],
              ).builder(),
            );
          },
        ),
      ),
    );
  }
}
