/// main_page
import 'dart:async';
import 'package:tech/index.dart';

class MainPage extends StatefulWidget {
  static const String routeName = '/main_page';

  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

///extends State<MainPage>
class _MainPageState extends State<MainPage> {
  late StreamSubscription<EvenBusNotificationEvent> eventBusNotification;
  late StreamSubscription<OneForeLogout> eventBusForeLogout;

  @override
  void initState() {
    eventBusNotification = oneEventBus.on<EvenBusNotificationEvent>().listen((event) {
      print("EvenBusNotificationEvent ::::::::::: ${event.type.name}");
      print(event.title);
      if (event.type != EnumNotificationActionType.onLocalBannerStatusBarPressed) {
        /// code
      }
    });
    eventBusForeLogout = oneEventBus.on<OneForeLogout>().listen((event) {
      if (event.isLogout) {
        //OneUserSharedInfo.logOut(context);
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    eventBusForeLogout.cancel();
    eventBusNotification.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return HomePage();
  }
}
