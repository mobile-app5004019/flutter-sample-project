import 'package:tech/index.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';

class SplashScreenPage extends StatefulWidget {
  static const String routeName = '/';

  const SplashScreenPage({Key? key}) : super(key: key);

  @override
  State<SplashScreenPage> createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 1200), () {
      _init(context).then((_) {
        Get.offAllNamed(HomePage.routeName);
      });
    });
    super.initState();
  }

  Future<void> _init(BuildContext context) async {
    await AppService().init();
    await RegisterOnePackage().init();
    FlutterNativeSplash.remove();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: oneBackgroundColor(context),
      body: Center(
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: SizedBox(),
            ),
            Center(
              child: OneWidgetSvgImage(
                fit: BoxFit.contain,
                width: Get.width * 0.5,
                path: OneSvg.logo,
                color: CustomColor.primary,
              ),
            ),
            Expanded(flex: 2, child: SizedBox()),
            OneLoadingContainer(),
            SizedBox(
              height: 60 + MediaQuery.of(context).padding.bottom,
            ),
          ],
        ),
      ),
    );
  }
}
